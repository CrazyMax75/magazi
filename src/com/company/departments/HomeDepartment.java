package com.company.departments;

import com.company.goods.BaseGoods;
import com.company.interfaces.IEmployee;
import com.company.interfaces.IGood;
import com.company.service.BaseEmployee;

import java.util.ArrayList;

public class HomeDepartment extends BaseDepartment{
    public HomeDepartment(String name, ArrayList<IGood> goodsList, ArrayList<IEmployee> employeesList) {
        super(name, goodsList, employeesList);
    }

    public HomeDepartment() {
        super();
    }
}



