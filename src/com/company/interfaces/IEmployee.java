package com.company.interfaces;

public interface IEmployee {
    boolean isFree();
    String getName();
    IDepartment getDepartment();
}


