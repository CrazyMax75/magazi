package com.company.service;

import com.company.departments.BaseDepartment;
import com.company.enums.ConsultResult;
import com.company.interfaces.IDepartment;
import com.company.interfaces.IVisitor;

public class Consultant extends BaseEmployee{

    public Consultant(String name, boolean free, IDepartment department) {
        super(name, free, department);
    }

    public Consultant(boolean free) {
        super(free);
    }

    public ConsultResult consult(IVisitor visitor) {
        super.setFree(false);
        return ConsultResult.BUY;
    }

    public void send(){

    }


}

