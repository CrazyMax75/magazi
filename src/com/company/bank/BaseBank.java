package com.company.bank;

import com.company.interfaces.IBank;

public abstract class BaseBank implements IBank {
    private String name;
    private String creditDescription;

    public BaseBank(String name, String creditDescription) {
        this.name = name;
        this.creditDescription = creditDescription;
    }

    public String getName() {
        return name;
    }

    public String getCreditDescription() {
        return creditDescription;
    }

    public void checkInfo(){

    }

    public void giveCredit(){
    }
}



